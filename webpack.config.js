

const path = require('path')

const webpack = require('webpack')

const htmlWebpackPlugin = require('html-webpack-plugin')

// const VueLoaderPlugin = require(vue-loader)

module.exports = {
    entry: path.join(__dirname, './src/main.js'), //入口
    output: {
        path: path.join(__dirname, './dist'), //指定 打包好的文件，输出到哪个目录中去
        filename: 'bundle.js' //这是指定 输出的文件的名称
    },
    // devServer: {
    //     open: true, //自动打开浏览器
    //     port: 3000,  //设置启动端口
    //     contentBase: 'src', //指定托管目录
    //     hot: true, //启用热更新
    //     host: true
    // },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),

        //创建一个在内存中生成 html 页面的插件
        new htmlWebpackPlugin({
            //指定 模板页面，将来会根据指定的页面路径，去生成内存中的页面
            template: path.join(__dirname, './src/index.html'),
            filename: 'index.html'
        })
    ],
    module: {
        rules: [
            {test: /\.css$/, use: ['style-loader','css-loader']},
            {test: /\.less$/, use: ['style-loader','css-loader','less-loader']},
            {test: /\.scss$/, use: ['style-loader','css-loader','sass-loader']},
            {test: /\.(jpg|png|gif|bmp|jpeg)$/, use: 'url-loader'},
            {test: /\.(ttf|eot|svg|woff|woff2)$/, use: 'url-loader'},
            {test: /\.vue$/, loader : 'vue-loader'},
            {test: /\.js$/, use: 'babel-loader', exclude: /node_modules/ },
        ]
    }
}
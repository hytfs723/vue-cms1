//入口文件
import Vue from 'vue'

import VueRouter from 'vue-router'

import VueResource from 'vue-resource'

import router from './router.js'

import './lib/mui/css/mui.css'

import './lib/mui/css/icons-extra.css'

// import { Header, Swipe, SwipeItem, Button, Lazyload } from 'mint-ui'
// Vue.component(Header.name, Header);
// Vue.component(Swipe.name, Swipe);
// Vue.component(SwipeItem.name, SwipeItem);
// Vue.component(Button.name, Button);
// Vue.use(Lazyload);

import MintUI from 'mint-ui'
Vue.use(MintUI)
import 'mint-ui/lib/style.css'

Vue.use(VueRouter)
Vue.use(VueResource)

//注册 Vuex
import Vuex from 'vuex'
Vue.use(Vuex)

//检测 localstorage中是否存储的有购物车数据，如果有
var car = JSON.parse(localStorage.getItem('car') || '[]')


var store  = new Vuex.Store({
    state: {
        //this.$store.state.
        car: car //将购物车中的商品的数据，用一个数组存储起来，
    },
    mutations: {
        addToCar(state, goodsinfo){
            //假设在购物车中 没有找到对应的商品
            var flag = false
            //点击加入购物车，把商品信息，保存到store 中的car上
            state.car.some(item => {
                if(item.id == goodsinfo.id) {
                    item.count += parseInt(goodsinfo.count)
                    flag = true
                    return true
                }
            })

            //如果最终，循环完毕，得到的flag 还是false 则把商品数据直接push到购物车中
            if(!flag) {
                state.car.push(goodsinfo)
            }

            //当 更新car 之后，吧car数组存储到本地的 localStorage 中
            localStorage.setItem('car', JSON.stringify(state.car))
        },
        updateGoodsInfo(state, goodsinfo) {
            //修改购物车中商品的数量值
            state.car.some(item=> {
                if(item.id == goodsinfo.id) {
                    item.count = parseInt(goodsinfo.count)
                    return true 
                }
            })

            //当修改完商品的数量，把最新的数量保存在缓存中
            localStorage.setItem('car',JSON.stringify(state.car))
        },
        removeFormCar(state, id) {
            //根据ID 从store中的购物车中删除对应的那条商品数据
            state.car.some((item, i)=> {
                if(item.id == id) {
                    state.car.splice(i, 1)
                    return true
                }
            })

            //当修改完商品的数量，把最新的数量保存在缓存中
            localStorage.setItem('car',JSON.stringify(state.car))
        },
        updateGoodsSelected(state, info) {
            state.car.some(item=> {
                if(item.id == info.id) {
                    item.selected = info.selected
                }
            })
            //当最新的所有购物车的商品状态保存到缓存中
            localStorage.setItem('car',JSON.stringify(state.car))
        }
    },
    getters: {
        getAllCount(state) {
            var c = 0;
            state.car.forEach(item => {
                c += item.count
            })
            return c
        },
        getGoodsCount(state) {
            var o = {}
            state.car.forEach(item => {
                o[item.id] = item.count
            })
            return o
        },
        getGoodsSelected(state) {
            var o = {}
            state.car.forEach(item => {
                o[item.id] = item.selected
            })
            return o
        },
        getGoodsCountAndAmount(state) {
            var o = {
                count:0,
                amount: 0
            }

            state.car.forEach(item=> {
                if(item.selected) {
                    o.count += item.count
                    o.amount += item.price * item.count
                }
            })
            return o
        }
    }
})




//导入格式化时间的插件
import moment from 'moment'
// //全局的时间过滤器
Vue.filter('dataFormat', function (dataStr, pattern = "YYYY-MM-DD HH:mm:ss") {  
    return moment(dataStr).format(pattern)
})

//设置请求的根路径
Vue.http.options.root = 'http://127.0.0.1:8899';

//全局设置 POST 的时候 表单数据格式组件的形式
Vue.http.options.emulateJSON = true;

import app from './app.vue'

var vm = new Vue({
    el: '#app',
    data: {
        msg: '123'
    },
    render: c => c(app),
    router,
    store
})